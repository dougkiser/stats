# stats

A collection of methods used for statistical calculations.

### What is this repository for? ###

* To provide a lightweight and fast set of methods used for stat calculations.

### How do I get set up? ###

* npm i