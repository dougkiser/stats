import {sortInAscendingOrder} from '../utils';

export default values => sortInAscendingOrder(values)[values.length - 1];