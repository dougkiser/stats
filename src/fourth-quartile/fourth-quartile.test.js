import test from 'tape';
import secondQuartile from './index';

test('secondQuartile', t => {
  const input = [-5, -4, 7.5, 8.7, 3.4, 9.4, 0.8, 1.5, 2.6, 0.9, 0.6, 9.4, 8.4, 6.6];
  const output = secondQuartile(input);
  const expected = 9.4;
  t.equal(output, expected, 'expected fourth-quartile was returned');
  t.end();
});