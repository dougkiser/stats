import test from 'tape';
import sum from './index';

test('sum', t => {
  const input = [1, 5, 7, 3, 6, 3];
  const output = sum(input);
  const expected = 25;
  t.equal(output, expected, 'expected sum of values was returned');
  t.end();
});