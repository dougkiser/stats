import median from '../median';
import {sortInAscendingOrder} from '../utils';

export default values => median(sortInAscendingOrder(values));