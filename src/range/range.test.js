import test from 'tape';
import range from './index';

test('range', t => {
  const input = [1,3,5,9,4,1,3,13,15,2];
  const output = range(input);
  const expected = 14;
  t.equal(output, expected, 'expected range of values was returned');
  t.end();
});