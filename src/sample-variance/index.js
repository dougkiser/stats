import sumNthPowerDeviations from '../sum-nth-power-deviations';

export default values => sumNthPowerDeviations(values) / (values.length - 1);