import test from 'tape';
import sampleVariance from './index';

test('sampleVariance', t => {
  const input = [43, 23, 4, 23, 5 ,7, 3, 43];
  const output = sampleVariance(input);
  const expected = 286.4107142857143;
  t.equal(output, expected, 'expected sample variance was returned');
  t.end();
});