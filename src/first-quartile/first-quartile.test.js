import test from 'tape';
import firstQuartile from './index';

test('firstQuartile', t => {
  const input = [-5, -4, 7.5, 8.7, 3.4, 9.4, 0.8, 1.5, 2.6, 0.9, 0.6, 9.4, 8.4, 6.6];
  const output = firstQuartile(input);
  const expected = 0.8;
  t.equal(output, expected, 'expected first-quartile was returned');
  t.end();
});