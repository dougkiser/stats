import {sortInAscendingOrder} from '../utils';

export default values => {
  const sortedValues = sortInAscendingOrder(values);
  const firstHalf = sortedValues.slice(0, Math.floor(sortedValues.length / 2));
  const firstQuartile = firstHalf.slice(0, Math.ceil(firstHalf.length / 2));
  return firstQuartile[firstQuartile.length - 1];
};