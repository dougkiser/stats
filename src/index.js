import changeInValue from './change-in-value';
import sum from './sum';
import geometricMean from './geometric-mean';
import harmonicMean from './harmonic-mean';
import mean from './mean';
import median from './median';
import range from './range';
import sumNthPowerDeviation from './sum-nth-power-deviations';
import sampleVariance from './sample-variance';
import sampleStandardDeviation from './sample-standard-deviation';
import midRange from './mid-range';
import standardError from './standard-error';
import coefficientOfVariance from './coefficient-of-variance';
import firstQuartile from './first-quartile';
import secondQuartile from './second-quartile';
import thirdQuartile from './third-quartile';
import fourthQuartile from './fourth-quartile';

export {
  changeInValue,
  sum,
  geometricMean,
  harmonicMean,
  mean,
  median,
  range,
  sumNthPowerDeviation,
  sampleVariance,
  sampleStandardDeviation,
  midRange,
  standardError,
  coefficientOfVariance,
  firstQuartile,
  secondQuartile,
  thirdQuartile,
  fourthQuartile
};

export default {
  changeInValue,
  sum,
  geometricMean,
  harmonicMean,
  mean,
  median,
  range,
  sumNthPowerDeviation,
  sampleVariance,
  sampleStandardDeviation,
  midRange,
  standardError,
  coefficientOfVariance,
  firstQuartile,
  secondQuartile,
  thirdQuartile,
  fourthQuartile
};