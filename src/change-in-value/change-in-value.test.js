import test from 'tape';
import changeInValue from './index';

test('changeInValue', t => {
  const input = [1, 5, 7, 3, 6, 3];
  const output = changeInValue(input);
  const expected = [4, 2, -4, 3, -3];
  t.deepEqual(output, expected, 'expected change in values were returned');
  t.end();
});