export default values => {
  const len = values.length;
  const changes = [];

  for (let index = 0; index < len; index++) {
    if (index !== 0) {
      changes.push(values[index] - values[index - 1]);
    }
  }
  return changes;
};