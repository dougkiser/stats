import test from 'tape';
import standardError from './index';

test('standardError', t => {
  const input = [43, 23, 4, 23, 5 ,7, 3, 43];
  const output = standardError(input);
  const expected = 5.983422038074389;

  t.equal(output, expected, 'expected standard error was returned');
  t.end();
});