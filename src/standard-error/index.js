import sampleStandardDeviation from '../sample-standard-deviation';

export default values => sampleStandardDeviation(values) / Math.sqrt(values.length);