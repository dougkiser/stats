import median from './index';
import test from 'tape';

test('median', t => {
  const input = [13,23,12,44,55];
  let output = median(input);
  let expected = 23;
  t.equal(output, expected, 'expected median was returned');

  input.push(22.333);
  output = median(input);
  expected = 22.6665;
  t.equal(output, expected, 'expected median was returned');

  t.end();
});