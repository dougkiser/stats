import mean from '../mean';
import {sortInAscendingOrder} from '../utils';

export default values => {
  const valuesSize = values.length;
  const valuesSortedAsc = sortInAscendingOrder(values);
  const middleIndex = Math.floor(valuesSize / 2);
  return valuesSize % 2
    ? valuesSortedAsc[middleIndex]
    : mean([valuesSortedAsc[middleIndex - 1], valuesSortedAsc[middleIndex]]);
};