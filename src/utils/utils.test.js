import test from 'tape';
import {sortInAscendingOrder} from './index';

test('sortInAscendingOrder', t => {
  const input = [9,6,3,1,2,4,5,7,8,10];
  const output = sortInAscendingOrder(input);
  const expected = [1,2,3,4,5,6,7,8,9,10];
  t.deepEqual(output, expected, 'expected sort order was returned');
  t.end();
});