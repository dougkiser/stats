import {sortInAscendingOrder} from '../utils';

export default values => {
  const sortedValues = sortInAscendingOrder(values);
  const valuesSize = sortedValues.length;
  const secondHalf = sortedValues.slice(Math.ceil(valuesSize / 2), valuesSize);
  const thirdQuartile = secondHalf.slice(0, Math.ceil(secondHalf.length / 2));
  return thirdQuartile[thirdQuartile.length - 1];
};