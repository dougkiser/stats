import test from 'tape';
import coefficientOfVariance from './index';

test('coefficientOfVariance', t => {
  const input = [5, 5.5, 4.9, 4.85, 5.25, 5.05, 6.0];
  const output = coefficientOfVariance(input);
  const expected = 0.07835445172346879;
  t.equal(output, expected, 'expected coefficient of variance from values was returned');
  t.end();
});