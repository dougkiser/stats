import mean from '../mean';
import sampleStandardDeviation from '../sample-standard-deviation';

export default values => sampleStandardDeviation(values) / mean(values);