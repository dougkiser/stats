import sum from '../sum';

export default values => sum(values) / values.length;