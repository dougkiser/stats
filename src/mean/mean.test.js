import mean from './index';
import test from 'tape';

test('mean', t => {
  const input = [13,23,12,44,55];
  const output = mean(input);
  const expected = 29.4;
  t.equal(output, expected, 'expected mean was returned');
  t.end();
});