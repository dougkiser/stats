import test from 'tape';
import geometricMean from './index';

test('geometricMean', t => {
  const input = [13,23,12,44,55];
  const output = geometricMean(input);
  const expected = 24.419319662986016;
  t.equal(output, expected, 'expected geometric mean was returned');
  t.end();
});