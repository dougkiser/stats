export default values => Math.pow(
  values.reduce((result, value) => result * value, 1),
  1 / values.length
);