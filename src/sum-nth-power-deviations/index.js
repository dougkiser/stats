import mean from '../mean';

export default (values, power = 2) => values.reduce(
  (sumOfSquares, value) => sumOfSquares + Math.pow(value - mean(values), power),
  0
);