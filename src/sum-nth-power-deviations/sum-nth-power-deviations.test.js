import test from 'tape';
import sumNthPowerDeviations from './index';

test('sumNthPowerDeviations', t => {
  const input = [43, 23, 4, 23, 5 ,7, 3, 43];
  const output = sumNthPowerDeviations(input);
  const expected = 2004.875;
  t.equal(output, expected, 'expected sum of nth power deviations was returned');
  t.end();
});