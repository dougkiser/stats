import test from 'tape';
import sampleStandardDeviation from './index';

test('sampleStandardDeviation', t => {
  const input = [34, 54, 2, 34, 2, 45, 21, 34];
  const output = sampleStandardDeviation(input);
  const expected = 18.797796223417862;
  t.equal(output, expected, 'expected sample standard deviation of values was returned');
  t.end();
});