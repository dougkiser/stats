import sampleVariance from '../sample-variance';

export default values => Math.sqrt(sampleVariance(values));