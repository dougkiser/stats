import test from 'tape';
import harmonicMean from './index';

test('harmonicMean', t => {
  const input = [13,23,12,44,55];
  const output = harmonicMean(input);
  const expected = 20.437880608144493;
  t.equal(output, expected, 'expected harmonic mean was returned');
  t.end();
});