import test from 'tape';
import midRange from './index';

test('midRange', t => {
  const input = [1,2,3,4,5];
  const output = midRange(input);
  const expected = 2;
  t.equal(output, expected, 'expected mid-range of values was returned');
  t.end();
});