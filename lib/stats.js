(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("stats", [], factory);
	else if(typeof exports === 'object')
		exports["stats"] = factory();
	else
		root["stats"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sum = __webpack_require__(1);

var _sum2 = _interopRequireDefault(_sum);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (values) {
  return (0, _sum2.default)(values) / values.length;
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (values) {
  return values.reduce(function (result, value) {
    return result + value;
  }, 0);
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mean = __webpack_require__(0);

var _mean2 = _interopRequireDefault(_mean);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (values) {
  var power = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  return values.reduce(function (sumOfSquares, value) {
    return sumOfSquares + Math.pow(value - (0, _mean2.default)(values), power);
  }, 0);
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sumNthPowerDeviations = __webpack_require__(2);

var _sumNthPowerDeviations2 = _interopRequireDefault(_sumNthPowerDeviations);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (values) {
  return (0, _sumNthPowerDeviations2.default)(values) / (values.length - 1);
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sampleStandardDeviation = exports.sampleVariance = exports.sumNthPowerDeviation = exports.range = exports.median = exports.mean = exports.harmonicMean = exports.geometricMean = exports.sum = exports.changeInValue = undefined;

var _changeInValue = __webpack_require__(5);

var _changeInValue2 = _interopRequireDefault(_changeInValue);

var _sum = __webpack_require__(1);

var _sum2 = _interopRequireDefault(_sum);

var _geometricMean = __webpack_require__(6);

var _geometricMean2 = _interopRequireDefault(_geometricMean);

var _harmonicMean = __webpack_require__(7);

var _harmonicMean2 = _interopRequireDefault(_harmonicMean);

var _mean = __webpack_require__(0);

var _mean2 = _interopRequireDefault(_mean);

var _median = __webpack_require__(8);

var _median2 = _interopRequireDefault(_median);

var _range = __webpack_require__(10);

var _range2 = _interopRequireDefault(_range);

var _sumNthPowerDeviations = __webpack_require__(2);

var _sumNthPowerDeviations2 = _interopRequireDefault(_sumNthPowerDeviations);

var _sampleVariance = __webpack_require__(3);

var _sampleVariance2 = _interopRequireDefault(_sampleVariance);

var _sampleStandardDeviation = __webpack_require__(11);

var _sampleStandardDeviation2 = _interopRequireDefault(_sampleStandardDeviation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.changeInValue = _changeInValue2.default;
exports.sum = _sum2.default;
exports.geometricMean = _geometricMean2.default;
exports.harmonicMean = _harmonicMean2.default;
exports.mean = _mean2.default;
exports.median = _median2.default;
exports.range = _range2.default;
exports.sumNthPowerDeviation = _sumNthPowerDeviations2.default;
exports.sampleVariance = _sampleVariance2.default;
exports.sampleStandardDeviation = _sampleStandardDeviation2.default;
exports.default = {
  changeInValue: _changeInValue2.default,
  sum: _sum2.default,
  geometricMean: _geometricMean2.default,
  harmonicMean: _harmonicMean2.default,
  mean: _mean2.default,
  median: _median2.default,
  range: _range2.default,
  sumNthPowerDeviation: _sumNthPowerDeviations2.default,
  sampleVariance: _sampleVariance2.default,
  sampleStandardDeviation: _sampleStandardDeviation2.default
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (values) {
  var len = values.length;
  var changes = [];

  for (var index = 0; index < len; index++) {
    if (index !== 0) {
      changes.push(values[index] - values[index - 1]);
    }
  }
  return changes;
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (values) {
  return Math.pow(values.reduce(function (result, value) {
    return result * value;
  }, 1), 1 / values.length);
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (values) {
  return values.length / values.reduce(function (result, value) {
    return result + 1 / value;
  }, 0);
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mean = __webpack_require__(0);

var _mean2 = _interopRequireDefault(_mean);

var _utils = __webpack_require__(9);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (values) {
  var valuesSize = values.length;
  var valuesSortedAsc = (0, _utils.sortInAscendingOrder)(values);
  var middleIndex = Math.floor(valuesSize / 2);
  return valuesSize % 2 ? valuesSortedAsc[middleIndex] : (0, _mean2.default)([valuesSortedAsc[middleIndex - 1], valuesSortedAsc[middleIndex]]);
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sortInAscendingOrder = exports.sortInAscendingOrder = function sortInAscendingOrder(values) {
  return values.sort(function (a, b) {
    return a - b;
  });
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

exports.default = function (values) {
  return Math.max.apply(Math, _toConsumableArray(values)) - Math.min.apply(Math, _toConsumableArray(values));
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sampleVariance = __webpack_require__(3);

var _sampleVariance2 = _interopRequireDefault(_sampleVariance);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (values) {
  return Math.sqrt((0, _sampleVariance2.default)(values));
};

/***/ })
/******/ ]);
});
//# sourceMappingURL=stats.js.map